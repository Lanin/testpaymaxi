<?php
/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 27.07.2016
 * Time: 19:53
 */

namespace Block;


class ImageBlock extends Block
{
    private $src;
    private $alt;
    
    public function setSrc($src)
    {
        $this->src = $src;
    }

    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    public function __toString() {
        return "<img src='$this->src' alt='$this->alt'>";
    }

}