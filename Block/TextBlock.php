<?php
/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 27.07.2016
 * Time: 19:53
 */

namespace Block;


class TextBlock extends Block
{
    private $content;

    public function setContent($content)
    {
        $this->content = $content;
    }
    
    public function __toString() {
        return "<div id='$this->id' class='$this->class'>$this->content</div>";
    }
    
}