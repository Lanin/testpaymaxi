<?php
/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 27.07.2016
 * Time: 19:50
 */

namespace Block;


abstract class Block
{

    protected $id;
    protected $class;

    public function setClass($class)
    {
        $this->class = $class;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    

  

    public function render() {
        ob_end_flush();
        echo $this;
        ob_end_flush();
    }

}