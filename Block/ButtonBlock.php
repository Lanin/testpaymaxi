<?php
/**
 * Created by PhpStorm.
 * User: Evgeni
 * Date: 27.07.2016
 * Time: 19:53
 */

namespace Block;


class ButtonBlock extends Block
{
    private $name;
    private $type;
    private $value;


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
        

    public function __toString() {
        return "<button id='$this->id' class='$this->class' name='$this->name' type='$this->type' value='$this->value'>$this->content</button>";
    }
}